from PyQt5.QtCore import QObject, QThread, pyqtSignal
import settings, getActiveWindow as aw
import time
import keyboard
import pyperclip

#Main Thread Class : Does all the writing !
class Worker(QObject):
    finished=pyqtSignal()
    sRedo=pyqtSignal()
    sStop=pyqtSignal()
    sErr=pyqtSignal()

    def __init__(self, aSplittedString, parent=None):
        QThread.__init__(self, parent)
        self.aSplittedString = aSplittedString

    #Send message by copy-paste from the clipboard
    def sendMessage(self, str):

        #Check if Gw2 is still launched
        if aw.isWindowLaunched("Guild Wars 2"):
            window = aw.getWindows("Guild Wars 2")
            window[0].activate()

            #Make Gw2 active in case it is not
            #(Should normally be anyway, but just in case)
            if not aw.isWindowActive("Guild Wars 2"):
                window = aw.getWindows("Guild Wars 2")
                window[0].activate()
                time.sleep(0.2)
            
            #YES, I had to to this the hard way
            #The send() function & ctrl+v won't work with Gw2, idk why...
            #Puts the clipboard into a temporary variable before putting it back in after use
            #Must test if all kind of stuff in the clipboard is still kept (should do it normally)
            if aw.isWindowActive("Guild Wars 2"):

                keyboard.send(settings.SCHATINGAME)
                #time.sleep(0.075)

                pyperclip.copy(str)
                keyboard.press('ctrl')
                keyboard.press('v')
                time.sleep(0.075)

                keyboard.release('v')
                keyboard.release('ctrl')
                pyperclip.copy("")
                #time.sleep(0.075)
                
                keyboard.send(settings.SCHATINGAME)
        else:
            return 1
        return 0


    #Quick function to define the waiting time before sending another message
    #This is made to comply with the exceding message rules of Gw2
    def timeWait(self, iLenString):

        fSleepTime = settings.FMINTIME + iLenString * settings.FTIMEPERCHAR

        while fSleepTime > 0.0:

            time.sleep(0.01)
            if  settings.bPause:
                while settings.bPause:
                    time.sleep(0.01)
                    if fSleepTime > 0.0:
                        fSleepTime -= 0.01
                    if settings.bStop:
                        break
            if settings.bStop:
                break
            fSleepTime -= 0.01


    #Function that will send message and wait
    #Also check for triggers such as Pause & Redo
    def messageLobby(self):
        iArraySupervisor = 0

        while (iArraySupervisor < len(self.aSplittedString)
                and not settings.bStop
                ):

            err = self.sendMessage(self.aSplittedString[iArraySupervisor])

            if err == 1:
                self.sErr.emit()
                self.sStop.emit()
                break

            if iArraySupervisor + 1 < len(self.aSplittedString):
                self.timeWait(len(self.aSplittedString[iArraySupervisor]))

            iArraySupervisor += 1

            if settings.bStop:
                self.sStop.emit()
                break

            elif settings.bRedo:

                if iArraySupervisor > 0:
                    iArraySupervisor -= 1
                else :
                    iArraySupervisor = 0
                settings.bRedo = False
                self.sRedo.emit()
        self.finished.emit()