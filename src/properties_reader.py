import settings

#check if a line is not void or a comment
def removeCommentAndVoid(line):

    if line is None:
        return False
    elif line == '':
        return False
    elif line == '\n':
        return False
    elif line[0] == '#':
        return False
    else:
        return True

#Get the file info and return it as a string
def getFile(filename):
    propFile = ""
    try:
        propFile = open(filename, "r")
    except FileNotFoundError:
        return 1
    
    return propFile.read()

#Return a clean list of the string
def cleanStrToList(propFile):
    listPropFile = propFile.split('\n')
    listPropFile = list(filter(removeCommentAndVoid, listPropFile))
    i = 0
    while i < len(listPropFile):
        listPropFile[i] = listPropFile[i].replace(" ", '')
        i+= 1
    return listPropFile

#Tries to transform the string into a float
def stringToNumber(string):

    number = 0
    try:
        number = float(string)
    except ValueError:
        return -1
    if number < 0:
        number = 0
    return number
        

#Return a bool if string is 'true'
def stringToBool(string):

    str = string.lower()
    if str=='true' or str == '1' or str== 'yes' or str=='y':
        return True
    else:
        return False

#Split the list in two to differenciate value and parameters
def separateListInTwo(listPropFile):

    list1 = []
    list2 = []
    i = 0
    while i < len(listPropFile):
        separator = listPropFile[i].find('=')
        if separator != -1:
            list1.append(listPropFile[i][:separator])
            list2.append(listPropFile[i][separator+1:])
        i+=1
    listPropFile = [list(a) for a in zip(list1, list2)]
    return listPropFile

#Set the values in the settings file
def setSettings(listPropFile):

    err = [False, False]
    i = 0
    while i < len(listPropFile):

        if (listPropFile[i][0] == 'exit'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SEXIT = listPropFile[i][1]

        elif (listPropFile[i][0] == 'open-chatbox'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SNEWTEXT = listPropFile[i][1]

        elif (listPropFile[i][0] == 'write'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SCONFIRMTEXT = listPropFile[i][1]

        elif (listPropFile[i][0] == 'pause'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SPAUSE = listPropFile[i][1]

        elif (listPropFile[i][0] == 'stop'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SSTOP = listPropFile[i][1]

        elif (listPropFile[i][0] == 'redo'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SREDO = listPropFile[i][1]

        elif (listPropFile[i][0] == 'get-old-text'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SOLDTEXT = listPropFile[i][1]

        elif (listPropFile[i][0] == 'in-game-chatbox-key'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.SCHATINGAME = listPropFile[i][1]

        elif (listPropFile[i][0] == 'language'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.LANGUAGE = listPropFile[i][1]

        elif (listPropFile[i][0] == 'minimum-time'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            tmpNum = stringToNumber(listPropFile[i][1])
            if tmpNum == -1:
                err[0] = True
            else:
                settings.FMINTIME = tmpNum

        elif (listPropFile[i][0] == 'time-per-character'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            tmpNum = stringToNumber(listPropFile[i][1])
            if tmpNum == -1:
                err[1] = True
            else:
                settings.FTIMEPERCHAR = tmpNum

        elif (listPropFile[i][0] == 'change-background-on-random'
            and (listPropFile[i][1] != ''
            or listPropFile[i][1] != None)):
            settings.CHANGE_BACKGROUND_ON_RANDOM = stringToBool(listPropFile[i][1])

        i += 1
    return err

def propertiesManager():

    mainStr = getFile(settings.DPROPFILE)
    if isinstance(mainStr, int):
        return 1
    mainList = cleanStrToList(mainStr)
    mainList = separateListInTwo(mainList)
    err = setSettings(mainList)

    if err[0] == False and err[1] == False:
        return 0
    if err[0] == False and err[1] == True:
        return 2
    if err[0] == True and err[1] == False:
        return 3
    if err[0] == True and err[1] == True:
        return 4