from os import listdir
import getActiveWindow as aw
from sys import exit, argv
from PyQt5 import QtCore, QtGui, QtWidgets
from random import randint
import settings, splitter, xml_reader, properties_reader
from writer import Worker

import keyboard

class ChatBox(QtWidgets.QWidget):

    #Signal to change the Window
    callInfo = QtCore.pyqtSignal()
    callChat = QtCore.pyqtSignal()

    #Signal to change text
    setText = QtCore.pyqtSignal(str)
    
    #Signals handling keyboards events
    sPause = QtCore.pyqtSignal()
    sStop = QtCore.pyqtSignal()
    sRedo = QtCore.pyqtSignal()
    sOldText = QtCore.pyqtSignal()
    sExit = QtCore.pyqtSignal()
    sNewText = QtCore.pyqtSignal()
    sConfirmText = QtCore.pyqtSignal()

    def __init__(self):
        super(ChatBox, self).__init__()

        #Load Both UI & Load default stylesheet
        self.initUi()
        self.styleSheetLoader()

        #Init states
        self.InfoBoxLabel = settings.LPREFIX + settings.LREADY
        self.pause = False
        self.oldText = False
        self.stop = False
        self.redo = False
        self.threadWorking = False
        self.selectedChatImage = ""
        self.selectedInfoImage = ""

        #Init background chatbox images (must be AFTER init states)
        self.selectedChatImage = initBackground(self, settings.SIMGCHATFOLDER)

        #Init InfoBox while keeping it hiden
        self.infoBox = InfoBox(self)
        self.setText.emit(self.InfoBoxLabel)
        self.selectedInfoImage = initBackground(self, settings.SIMGINFOFOLDER)

        #Load Stylesheet
        self.setStyleSheet(self.stylesheet)

        #Grab all keypresses
        self.hook = keyboard.on_press(self.keyboardEventReceived)

        #Connect the changing of window event
        self.callInfo.connect(self.callInfoBox)

        #All connection events coming from the keyboard
        #Must do like this to avoid thread problems
        self.sPause.connect(self.eventPause)
        self.sStop.connect(lambda: self.eventStop(1))
        self.sRedo.connect(lambda: self.eventRedo(1))
        self.sOldText.connect(self.eventOldText)
        self.sExit.connect(self.eventExit)
        self.sNewText.connect(self.eventNewText)
        self.sConfirmText.connect(self.eventConfirmText)

        self.raise_()
        self.activateWindow()
        
    #Creates the ChatBox
    def initUi(self):

        self.setWindowTitle(settings.TCHATBOX)

        #Creates and set the editBox
        self.editBox = QtWidgets.QPlainTextEdit()
        boxLayout = QtWidgets.QVBoxLayout()
        boxLayout.addWidget(self.editBox)
        self.setLayout(boxLayout)
        boxLayout.setContentsMargins(0,0,0,0)

        #Set Icon
        try:
            icon = QtGui.QIcon(settings.EIMGICON)
            if icon != None:
                self.setWindowIcon(icon)
        except:
            raise

        #Force focus on top
        self.setWindowFlags( self.windowFlags() |
            QtCore.Qt.Window |
            QtCore.Qt.WindowStaysOnTopHint |
            QtCore.Qt.FramelessWindowHint |
            QtCore.Qt.X11BypassWindowManagerHint)

        #Other parameters
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        #Set the ChatBox at the bottom center of the screen
        self.size = QtWidgets.QDesktopWidget().screenGeometry(-1)
        self.screen_width = round(self.size.width() * 35 / 100)
        self.screen_height = round(self.size.height() * 15 / 100)
        self.setGeometry(
            round(self.size.width() / 2 - self.screen_width / 2),
            round(self.size.height() - self.screen_height - (self.size.height() * 1.25 / 100)),
            self.screen_width,
            self.screen_height)

    #Load Stylesheet in the folder images/infobox_backgrounds
    def styleSheetLoader(self):

        self.stylesheet = QtCore.QFile(settings.DQSSFILE)
        self.stylesheet.open(QtCore.QIODevice.ReadOnly)
        self.stylesheet = str(self.stylesheet.readAll(), encoding="utf-8")

    #Hide ChatBox and show InfoBox
    def callInfoBox(self):
        self.hide()
        if settings.CHANGE_BACKGROUND_ON_RANDOM:
            self.selectedInfoImage = changeBackground(self, settings.SIMGINFOFOLDER, self.selectedInfoImage)
        self.infoBox.show()

    #Set a specific text for the InfoBox
    def setTextInfoBox(self, text):
        self.InfoBoxLabel = text
        self.setText.emit(self.InfoBoxLabel)

    #Update first line (for time)
    def updateTextInfoBox(self, text, type = settings.LWRITE):
        if self.InfoBoxLabel.find('\n') == -1 :
            self.InfoBoxLabel = settings.LPREFIX + type + text
        else:
            tmpList = self.InfoBoxLabel.split('\n')
            tmpList[0] = settings.LPREFIX + type + text
            i = 1
            while i < len(tmpList):
                tmpList[i] = '\n' + tmpList[i]
                i += 1
            self.InfoBoxLabel = ''.join(tmpList)
        self.setText.emit(self.InfoBoxLabel)

    #Remove a specific text for the InfoBox
    def removeTextInfoBox(self, text):
        self.InfoBoxLabel = self.InfoBoxLabel.replace(text, '')
        self.setText.emit(self.InfoBoxLabel)
    
    #Add a specific text for the InfoBox
    def addTextInfoBox(self, text):
        self.InfoBoxLabel = self.InfoBoxLabel + text
        self.setText.emit(self.InfoBoxLabel)

    #############################
    # Event manager starts here #
    #############################

    #Pause event > Pause the writing at any moment
    def eventPause(self):
        #Only works if it's writing
        if self.threadWorking:
            if not self.pause:
                self.pause = True
                settings.bPause = True
                self.addTextInfoBox(settings.LPREFIXCRLF + settings.LPAUSE)
            else:
                self.pause = False
                settings.bPause = False
                self.removeTextInfoBox(settings.LPREFIXCRLF + settings.LPAUSE)

    #Stop event > Stop the writing at any state
    def eventStop(self, state):
        #Only works if it's writing
        if self.threadWorking:
            if not self.stop and state == 1:
                self.addTextInfoBox(settings.LPREFIXCRLF + settings.LSTOP)
                self.stop = True
                settings.bStop = True
            elif state == 2 and settings.bStop:
                self.removeTextInfoBox(settings.LPREFIXCRLF + settings.LSTOP)
                self.addTextInfoBox(settings.LPREFIXCRLF + settings.LSTOPPED)
                self.removeTextInfoBox(settings.LPREFIXCRLF + settings.LREDO)
                settings.bRedo = False
                settings.bStop = False
                if self.pause:
                    self.eventPause()
    
    #Redo event > Redo the last message as a counter to the anti-spam system
    def eventRedo(self, state):
        #Only works if it's writing
        if self.threadWorking:
            if not self.redo and state == 1:
                self.addTextInfoBox(settings.LPREFIXCRLF + settings.LREDO)
                self.redo = True
                settings.bRedo = True
            elif state == 2:   
                self.removeTextInfoBox(settings.LPREFIXCRLF + settings.LREDO)
                self.redo = False
                settings.bRedo = False

    #OldText event > Keep the text once it's pressed
    def eventOldText(self):
        if not self.oldText:
            self.addTextInfoBox(settings.LPREFIXCRLF + settings.LLAST)
        self.oldText = True

    #Exit event > Quit the program
    def eventExit(self):
        self.close()

    #NewText event > Open the Chatbox and awaits for input
    def eventNewText(self):
        if not self.oldText or self.editBox.toPlainText() == "\n":
            self.editBox.setPlainText("")
        else:
            self.editBox.setPlainText(self.editBox.toPlainText()[:-1])
            self.editBox.moveCursor(QtGui.QTextCursor.End)
        self.callChat.emit()

    #Confirm event > Reset the states and does start writing thread if needed
    def eventConfirmText(self):

        self.oldText = False
        self.pause = False  
        settings.bPause = False
        self.stop = False
        settings.bStop = False
        self.redo = False  
        settings.bRedo = False 
        self.callInfo.emit()
        sMessage = self.editBox.toPlainText()
        
        aMessage = splitter.splitText(sMessage)
        if aMessage:
            self.setTextInfoBox(settings.LPREFIX + settings.LWRITE)
            self.startWritingThread(aMessage)
        else:
            self.setTextInfoBox(settings.LPREFIX + settings.LREADY)

    #Informs the writing is finished and ready for another writing request
    def endOfWriting(self):
        self.removeTextInfoBox(settings.LPREFIX + settings.LWRITE)
        self.InfoBoxLabel = settings.LPREFIX + settings.LREADY + self.InfoBoxLabel
        self.setTextInfoBox(self.InfoBoxLabel)
        self.threadWorking = False

    #Start the main writing Thread
    def startWritingThread(self, aMessage):

        self.threadWorking = True
        self.thread=QtCore.QThread()
        self.worker=Worker(aMessage)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.messageLobby)
        self.worker.sRedo.connect(lambda: self.eventRedo(2))
        self.worker.sStop.connect(lambda: self.eventStop(2))
        self.worker.sErr.connect(lambda: errorBox(settings.GWNOTFOUND, 1))
        self.worker.finished.connect(self.endOfWriting)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.start()

    ################################
    # Keyboard manager starts here #
    ################################

    def keyboardEventReceived(self, event):
        #Pause
        if keyboard.is_pressed(settings.SPAUSE):
            if self.isHidden() and aw.isWindowActive(settings.GWNAME):
                self.sPause.emit()

        #Stop
        if keyboard.is_pressed(settings.SSTOP):
            if self.isHidden() and aw.isWindowActive(settings.GWNAME):
                self.sStop.emit()
        
        #Redo
        if keyboard.is_pressed(settings.SREDO):
            if aw.isWindowActive(settings.GWNAME):
                self.sRedo.emit()

        #Keep old text
        if keyboard.is_pressed(settings.SOLDTEXT):
            if self.isHidden() and aw.isWindowActive(settings.GWNAME):
                self.sOldText.emit()
            
        #Exit
        if keyboard.is_pressed(settings.SEXIT):
            self.sExit.emit()
        
        #Open ChatBox
        if keyboard.is_pressed(settings.SNEWTEXT):
            if (self.isHidden()
                and self.threadWorking == False
                and aw.isWindowActive(settings.GWNAME)):
                self.sNewText.emit()

        #Confirm Chatbox
        if (keyboard.is_pressed(settings.SCONFIRMTEXT)
            and not keyboard.is_pressed('shift')
            and aw.isWindowActive(settings.TCHATBOX)):
            if self.isVisible():
                self.sConfirmText.emit()

class InfoBox(QtWidgets.QWidget):
    def __init__(self, parent):
        super(InfoBox, self).__init__(parent)

        #Init and place the UI
        self.initUi()
        self.setLabelPos()

        #Connect the events (change of window & message)
        self.chatbox = parent

        self.chatbox.callChat.connect(self.callChatBox)
        self.chatbox.setText.connect(self.setMessage)

    #Init the InfoBox
    def initUi(self):

        
        self.setWindowTitle(settings.TINFOBOX)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.labelInfo = QtWidgets.QLabel(self)

        #Create the label
        infoLayout = QtWidgets.QVBoxLayout()
        infoLayout.addWidget(self.labelInfo)
        self.setLayout(infoLayout)

        #Force on top
        self.setWindowFlags( self.windowFlags() |
            QtCore.Qt.Window |
            QtCore.Qt.WindowStaysOnTopHint |
            QtCore.Qt.FramelessWindowHint |
            QtCore.Qt.X11BypassWindowManagerHint)

    #Set the InfoBox at the bottom center of the screen
    def setLabelPos(self, lblWidth=240, lblHeight=16):

        self.size = app.primaryScreen().size()

        width = round(self.size.width() * 50 / 100)
        height = round(self.size.height() * 87.5 / 100)
        self.setGeometry(round(width - lblWidth / 2), round(height - lblHeight+20), round(lblWidth+20), round(lblHeight+20))
        
    #Set a message in the InfoBox and resize it
    def setMessage(self, message):
        self.labelInfo.setText(message)
        self.labelInfo.adjustSize()
        self.setLabelPos(self.labelInfo.size().width(), self.labelInfo.size().height())

    #Hide the InfoBox and show the ChatBox
    def callChatBox(self):
        self.hide()
        if settings.CHANGE_BACKGROUND_ON_RANDOM:
            self.chatbox.selectedChatImage = changeBackground(self.chatbox, settings.SIMGCHATFOLDER, self.chatbox.selectedChatImage)
        self.chatbox.raise_()
        self.chatbox.activateWindow()
        self.chatbox.show()

#Load image, check for errors
def loadImages(selectedDir):

    compatibleFormat = settings.COMPATIBLEFORMAT
    myImages = []
    try:
        myImages = listdir(selectedDir)
    except(FileNotFoundError):
        return 1

    if myImages == []:
        return 2

    i = 0
    while i < len(myImages):

        spliter = myImages[i].split('.')
        if len(spliter) < 2:
            del myImages[i]
            i -= 1
        elif (len(spliter) > 1 and not spliter[-1] in compatibleFormat):
            del myImages[i]
            i -= 1
        i += 1
    return myImages

#Change background on the fly
def changeBackground(self, selDir, selectedImage):
    
    if self.stylesheet.find(selectedImage) != -1:
        listOfImages = loadImages(selDir)
        if isinstance(listOfImages, int):
            return selectedImage
        elif not listOfImages == []:
            tmpImage = selDir + listOfImages[randint(0, len(listOfImages)-1)]
            self.stylesheet = self.stylesheet.replace(selectedImage, tmpImage)
    self.setStyleSheet(self.stylesheet)
    return(tmpImage)

#Load background on the first run
def initBackground(self, selDir):

    listOfImages = loadImages(selDir)
    if isinstance(listOfImages, int):
        if listOfImages == 1:
            errorBox(settings.ERRFOLDER, 2)
        elif listOfImages == 2:
            errorBox(settings.ERRFILE, 2)
        else:
            errorBox(settings.ERRUNKNOW, 1)

    elif not listOfImages == []:
        selectedImage = selDir + listOfImages[randint(0, len(listOfImages) -1)]
        self.stylesheet = self.stylesheet.replace("background-color: #101A1A;",
                                                "background-image: url("+selectedImage+");", 1)

        return selectedImage
    return None

#Check for error with XML file
def errXmlManager():
    err = xml_reader.xmlManager()

    if err[0] == 1:
        errorBox(settings.XERRFILE, 2)
    elif err[0] == 2:
        errorBox(settings.XERRPARSE, 2)
    elif err[0] == 3:
        errorBox(settings.XERRLANG, 2)
    elif err[0] == 0 and err[1]:
        errorBox(settings.XWARNCORRUPT,2)

#Check the properties file and makes the changes on settings
def propertiesLoader():

    err = properties_reader.propertiesManager()
    if err == 1:
        errorBox(settings.ERRLOADPROP, 2)
    elif err == 2:
        errorBox(settings.ERRNUMCMIN, 2)
    elif err == 3:
        errorBox(settings.ERRNUMTMIN, 2)
    elif err == 4:
        errorBox(settings.ERRNUMBOTH, 2)

#Show error message       
def errorBox(message, type = 0):

    try:
        icon = QtGui.QIcon(settings.EIMGICON)
    except FileNotFoundError:
        raise

    if type == 1:
        type = QtWidgets.QMessageBox.Critical
    elif type == 2:
        type = QtWidgets.QMessageBox.Warning
    elif type == 3:
        type = QtWidgets.QMessageBox.Information
    else:
        type = QtWidgets.QMessageBox.NoIcon

    errMessage = QtWidgets.QMessageBox()
    errMessage.setWindowTitle(settings.TERRBOX)
    if icon != None:
        app.setWindowIcon(icon)
        errMessage.setWindowIcon(icon)
    errMessage.setIcon(type)
    errMessage.setInformativeText(message)
    errMessage.setStandardButtons(QtWidgets.QMessageBox.Ok)
    errMessage.exec_()

app = QtWidgets.QApplication(argv)

if not aw.isWindowLaunched(settings.GWNAME):

    errorBox(settings.GWNOTFOUND, 1)
    exit()


propertiesLoader()
errXmlManager()
winChatBox = ChatBox()
winChatBox.setWindowIcon(QtGui.QIcon(settings.EIMGICON))
winChatBox.show()

exit(app.exec_())