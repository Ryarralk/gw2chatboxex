import xml.etree.ElementTree as ET
import settings

#Get name and traduction in a 2D list
def readMe(selLang):

    tmpList = []
    tmpList2 = []
    try:
        tree= ET.parse(settings.DXMLFILE)
    except(FileNotFoundError):
        return 1
    except(ET.ParseError):
        return 2
    root= tree.getroot()
    for name in root:
        for message in name:
            tmpList.append(message.attrib['name'])
            
            for lang in message.findall(selLang):
                tmpList2.append(lang.text)

    tmpList3 = [list(a) for a in zip(tmpList, tmpList2)]
    return tmpList3

#Get all available languages
def getLangList():

    langList = []
    try:
        tree= ET.parse(settings.DXMLFILE)
    except(FileNotFoundError):
        return 1
    except(ET.ParseError):
        return 2
    root = tree.getroot()
    for name in root:
        for message in name:
            for lang in message:
                if lang.tag not in langList:
                    langList.append(lang.tag)

    return langList

#Change constants in settings
def modifySettings(wordList):

    errFound = False
    i = 0
    while i < len(wordList):

        if wordList[i][0] == 'ready':
            if not wordList[i][1] == None:
                settings.LREADY = wordList[i][1]
        elif wordList[i][0] == 'writing':
            if not wordList[i][1] == None:
                settings.LWRITE = wordList[i][1]
        elif wordList[i][0] == 'pause':
            if not wordList[i][1] == None:
                settings.LPAUSE = wordList[i][1]
        elif wordList[i][0] == 'stopping':
            if not wordList[i][1] == None:
                settings.LSTOP = wordList[i][1]
        elif wordList[i][0] == 'stopped':
            if not wordList[i][1] == None:
                settings.LSTOPPED = wordList[i][1]
        elif wordList[i][0] == 'redo':
            if not wordList[i][1] == None:
                settings.LREDO = wordList[i][1]
        elif wordList[i][0] == 'last':
            if not wordList[i][1] == None:
                settings.LLAST = wordList[i][1]
        elif wordList[i][0] == 'errFolder':
            if not wordList[i][1] == None:
                settings.ERRFOLDER = wordList[i][1]
        elif wordList[i][0] == 'errFile':
            if not wordList[i][1] == None:
                settings.ERRFILE = wordList[i][1]
        elif wordList[i][0] == 'errUnknown':
            if not wordList[i][1] == None:
                settings.ERRUNKNOW = wordList[i][1]
        elif wordList[i][0] == 'warnCorrupt':
            if not wordList[i][1] == None:
                settings.XWARNCORRUPT = wordList[i][1]
        elif wordList[i][0] == 'xErrFile':
            if not wordList[i][1] == None:
                settings.XERRFILE = wordList[i][1]
        elif wordList[i][0] == 'xErrParse':
            if not wordList[i][1] == None:
                settings.XERRPARSE = wordList[i][1]
        elif wordList[i][0] == 'xErrLang':
            if not wordList[i][1] == None:
                settings.XERRLANG = wordList[i][1]
        elif wordList[i][0] == 'errGw2NotFound':
            if not wordList[i][1] == None:
                settings.GWNOTFOUND = wordList[i][1]
        else:
            errFound = True
        i += 1
    return errFound

#Check for empty (None) values
def checkForNone(dataList):

    for name in dataList:
        if name == None:
            return True
        for translation in name:
            if translation == None:
                return True
    return False

#Retrive the data, use it & check for errors
def xmlManager():
    
    lang = settings.LANGUAGE
    dataList = []
    possibleCorruption = False
    langList = getLangList()
    if isinstance(langList, int):
        return langList, possibleCorruption
    elif not lang in langList:
        return 3, possibleCorruption
    else:
        dataList = readMe(lang)
        if isinstance(dataList, int):
            return dataList, possibleCorruption
        else:
            possibleCorruption += checkForNone(dataList)
            possibleCorruption += modifySettings(dataList)
    
    return 0, possibleCorruption