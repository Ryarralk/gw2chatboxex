import pygetwindow as gw

def isWindowLaunched(name):
    try:
        win = gw.getWindowsWithTitle(name)
        if win:
            return True
        else:
            return False
    except IndexError:
        return False

def isWindowActive(name):
    try:
        if gw.getActiveWindowTitle() == name:
            return True
        return False
    except IndexError:
        return False

def activateWindow(name, instance):
    try:
        window = gw.getWindowsWithTitle(name)[instance]
        window.activate()
        return True
    except IndexError:
        return False
    

def getWindows(name):
    if isWindowLaunched(name):
        return gw.getWindowsWithTitle(name)
    else:
        return False

if __name__ == "__main__" :

    print(isWindowActive("Guild Wars 2"))
    print(isWindowLaunched("Guild Wars 2"))

    window = getWindows("Guild Wars 2")
    print(window)
    #window[0].activate()



    print (isWindowActive("Guild Wars 2"))
    print (gw.getWindowsWithTitle('Guild Wars 2'))