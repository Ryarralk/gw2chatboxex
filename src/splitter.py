#Removes the surplus of newlines to avoid spam and "bugs"
def removeMultipleNewLines(aString):

    iStrLen = len(aString)
    i = 0

    while i < iStrLen:

        #Removes '\n' at the start of the list
        if i == 0 and aString[i] == '\n':
            del aString[i]
            return(removeMultipleNewLines(aString))
        #Removes '\n' at the end of the list
        elif i == iStrLen - 1 and aString[i] == '\n':
            del aString[i]
            return(removeMultipleNewLines(aString))
        #Removes '\n' in surplus
        elif aString[i] == '\n' and i + 1 < len(aString):
            if aString[i + 1] == '\n':
                del aString[i+1]
                iStrLen = len(aString)
                return(removeMultipleNewLines(aString))
        i += 1

    return(aString)

# /!\ Main function of splitter /!\
#Splits the message into multiple ones
def splitText(str):

    aString = list(str)
    aSplittedString = []
    sBuffer = ""
    sEmote = ""
    bFirstPass = True
    PONCTUATION = ["?", ",", ";", ".", ":", "!", "*", ">"]

    bWriting = True

    if len(aString) == 0:
        return 0
    else:
        aString = removeMultipleNewLines(aString)
        if len(aString) == 0:
            return 0

    #While main array not removed completely (removes it a bit after
    #each writing segments)
    while len(aString) > 0 and bWriting == True:

        #If text < 199 char send without modification
        if len(aString) < 199:
            
            sBuffer += ''.join(aString)
            aString = []
            aSplittedString.append(sBuffer)

        #Else if have at least one new line
        elif aString.count("\n") > 0 and ''.join(aString).find("\n", 0, 185) != -1:

            #Check for emotes
            if bFirstPass == True:
                if aString[0] == '/':
                    sEmote = ''.join(aString[0 : aString.index(" ") + 1])

            sBuffer += ''.join(aString[0 : aString.index('\n', 0, 185)])
            sBuffer += " --"
            aString = aString[aString.index('\n', 0, 185)+1:]
            aSplittedString.append(sBuffer)
            sBuffer = sEmote + "-- "

        #Else if text have at least one space
        elif aString.count(" ") > 2 and ''.join(aString).find(" ", 0, 185) != -1:

            #Keep the index of last, current and future space 
            #if ponctuation detected after current space
            #(and the last of the segment)

            iSpaceLast = -1
            iSpace = -1
            iSpaceNext = -1

            #Check for emotes
            if bFirstPass == True:
                if aString[0] == '/':
                    sEmote = ''.join(aString[0 : aString.index(" ") + 1])
            
            #Get the last two spaces of the text segment, iSpaceLast = -1 if there's only one space
            for i in range(0, 185):
                if aString[i] == " ":
                    iSpaceLast = iSpace
                    iSpace = i

            #Check for ponctuation after iSpace
            for i in PONCTUATION:
                if aString[iSpace + 1] == i:
                    for j in range(iSpace + 1, 190):
                        if aString[j] == " ":
                            iSpaceNext = j
                    break

            #If iSpaceNext != 1 then go for iSpaceNext, Elif no other space but iSpaceLast != -1 then back to iSpaceLast, Else go full monke !
            if iSpaceNext != -1:
                iSpace = iSpaceNext
            elif iSpaceNext == -1 and iSpaceLast != -1:
                iSpace = iSpaceLast
            else:
                iSpace = 190

            sBuffer += ''.join(aString[0 : iSpace + 1])
            sBuffer += "--"
            aString = aString[iSpace:]
            aSplittedString.append(sBuffer)
            
            sBuffer = sEmote + "--"

        #Else the text has no space
        else:

            if bFirstPass == True:
                if aString[0] == "/":
                    sEmote = ''.join(aString[0 : aString.index(" ") + 1])

            sBuffer += ''.join(aString[ : 185] )
            sBuffer += " --"
            aString = aString[185: ]

            aSplittedString.append(sBuffer)

            sBuffer = sEmote + "-- "

        bFirstPass = False

    return aSplittedString