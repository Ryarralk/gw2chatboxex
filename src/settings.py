#Settings file keeping variables to make everything communicate


#Keyboard Events for writer
#Required for the thread.
bPause = False
bStop = False
bRedo = False

#Default keyboard Keys

SPAUSE = 'f9'
SSTOP = 'f10'
SREDO = 'f8'
SOLDTEXT = 'f7'
SEXIT = 'shift+esc'
SNEWTEXT = 'shift+ctrl'
SCONFIRMTEXT = 'enter'

SCHATINGAME = 'enter'

IMGFOLDER = 'images/'

SIMGCHATFOLDER = IMGFOLDER + 'chatbox_backgrounds/'
SIMGINFOFOLDER = IMGFOLDER + 'infobox_backgrounds/'

#Parameters

LANGUAGE = 'en'
CHANGE_BACKGROUND_ON_RANDOM = False

#Waiting Time
FMINTIME = 0.00
FTIMEPERCHAR = 0.00

#Default Labels

#Titles
TERRBOX = "Gw2 ChatBoxEx"
TCHATBOX = "Gw2 ChatBoxEx - ChatBox"
TINFOBOX = "Gw2 ChatBoxEx - InfoBox"

#InfoBox
LPREFIX = "> "
LPREFIXCRLF = "\n" + LPREFIX

LREADY = "Ready..."
LWRITE = "Writing..."
LPAUSE = "Pause..."
LSTOP = "Stopping..."
LSTOPPED = "Stopped!"
LREDO = "Waiting for redo..."
LLAST = "Last text applied."

#ErrorBox
EIMGICON = IMGFOLDER + 'icons/chatboxex.ico'

#Main
ERRFOLDER = "Image folder not found!"
ERRFILE = "No image found!"
ERRUNKNOW = "If you see this error, let my creator know!"

#PROPERTIES
PFILE = 'chatboxex.properties'
ERRLOADPROP = 'Property file not found!\nMake sure the name of the file is:'+' '+PFILE
ERRNUMTMIN = "Unable to load 'minimum-time' parameter. Make sure you have put a number " +\
                "and that you used '.' instead of ','."
ERRNUMCMIN = "Unable to load 'time-per-character' parameter. Make sure you have put a number " +\
                "and that you used '.' instead of ','."
ERRNUMBOTH = "Unable to load 'time-per-character' & 'minimum-time' parameter. Make sure that " +\
               "you have put a number and that you used '.' instead of ','."

#XML
XFILE = "languages.xml"
XWARNCORRUPT = 'Corruption detected in XML file. Some informative text may not be the one excpected.'
XERRFILE = 'XML file not found! Make sure the name of the file is:'+' '+XFILE
XERRPARSE = 'Error while reading the XML file! File may be corrupt or it could be the wrong one.'
XERRLANG = 'Error while trying to get the lang selected! Please, make sure that you typed it well. '+\
            'If the problem persists, please, get in touch with my creator.'

#GW2
GWNAME = "Guild Wars 2"
GWNOTFOUND = "Error while trying to find Guild Wars 2 client! Please, make sure the game is launched "+\
            "and is in windowed or in fullscreen windowed mode."

#Data Folder

QFILE = 'style.qss'

DFOLDER = 'data/'
DXMLFILE = DFOLDER + XFILE
DPROPFILE = DFOLDER + PFILE
DQSSFILE = DFOLDER + QFILE


#LoadImage
COMPATIBLEFORMAT = ['bmp', 'cur', 'gif', 'icns', 'ico',
                    'jpeg', 'jpg', 'pbm', 'pgm', 'png',
                    'ppm', 'svg', 'svgz', 'tga', 'tif',
                    'tiff', 'wbmpp', 'webp', 'xbm', 'xpm']