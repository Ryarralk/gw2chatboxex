# Gw2 - ChatBoxEx
```
 _____            _____            _____  _             _   ______               _____       
|  __ \          / __  \          /  __ \| |           | |  | ___ \             |  ___|      
| |  \/__      __`' / /'  ______  | /  \/| |__    __ _ | |_ | |_/ /  ___  __  __| |__  __  __
| | __ \ \ /\ / /  / /   |______| | |    | '_ \  / _` || __|| ___ \ / _ \ \ \/ /|  __| \ \/ /
| |_\ \ \ V  V / ./ /___          | \__/\| | | || (_| || |_ | |_/ /| (_) | >  < | |___  >  < 
 \____/  \_/\_/  \_____/           \____/|_| |_| \__,_| \__|\____/  \___/ /_/\_\\____/ /_/\_\
```                                                                                             
                                                                                             
## Introduction

Guild Wars 2® is a great game with a non-negligeable Role-Player community. ArenaNet gave some improvements in terms of quality of life for the Role-players, but it is not enough. As a Game Master, I have often found myself struggling with the limited in-game chat box.\
The principal problem with it is the lack of length. In fact, you can only type 199 characters per messages.\
This limit helps to reduces the quantity of random and useless text send by some trolls or malicious people. But, on the other side, it puts a heavy limit on the text a Role-Player wants to write.\
For this reason, I have decided to create Gw2 - ChatBoxEx!

## Requirement

To use Gw2 - ChatBoxEx, you must have Guild Wars 2 launched in Windowed or Windowed Fullscreen. The program will not work properly if your game is in Fullscreen mode.\
For more comfort of use while using the program, it is recommended to disable the option “Mute when GW2 is in the background”.

## Warning

Be aware that I cannot be held responsible on how you are using this software or the consequences that goes with by using it. This tool uses the minimum interaction possible with the game and only presses keys. It uses the same principle as the MusicBox software. Gw2 - ChatBoxEx has been designed with the heart and the will of making the Role-Playing community of Guild Wars 2 a better place. I condemn any malicious usage of this software.

## How it works

### The problem

The idea behind Gw2 – ChatBoxEx is simple. How to send a huge wall text without the 199 characters limits?\
The first idea would be to go directly into the file of the game and remove this limit. But this would go against the Terms of Service of the game itself, plus, this could lead to instability and vulnerabilities.\
The most thoughtful approach would require the next prerequisites:

- Respecting the Terms of Service of the game.
- Having a chat box longer that could hold more than 199 characters.
- Sending all the written text into the game by a way or another.
- Respecting the “anti-spam” system of the in-game chat box.
- Have a minimal impact on the comfort of the user.

To respect the terms of services, we must understand a couple of things:
- We must not modify in any way the data of the game.
- We must not make macro that could offer an advantage to another player.

### The solution

Gw2 - ChatBoxEx had been created with those restrictions in mind. For this, the program is entirely independent and does the minimum possible interaction with the game, that is… Writing.

Here is how it works:

1. A chat box opens and stay on top of the screen whatever the application is behind. It takes the place of the skill bar. The chat box has an “unlimited” capacity.
2. The text is written down inside the chat box and the user confirms the action to write it in-game, the program will split the text into multiple parts. Those part are less than 199 characters each.
3. The program will send the data through the game by using the clipboard. This way, there is only 3 actions per-messages: 
    1. Open the chat box.
    2. Paste the part of the text.
    3. Send the message.
4. A customizable time is set for the program to wait until sending the next message. This waiting time is very important to avoid the game to consider your message as a spam because a wall of text is sent instantly through the chat box.
5. It repeats the actions 3 & 4 until all the text is sent or until the user orders to stop.

## Overview

The User Interface of Gw2 - ChatBoxEx is simple. It consists of only two windows:

- The ChatBox: This is the main part of the program. It replaces the in-game chat box.
- The InfoBox: This smaller box gives multiples information such as Writing, Pause, Redo, etc…
The Chat Box

![ChatBox](readme_img/chatbox.png "ChatBox")

The Info Box
![InfoBox](readme_img/infobox.png "InfoBox")

## Features

Gw2 - ChatBoxEx has multiple features to offer:

- Convert a text into multiple slices to write it into the in-game chat box.
- Emote support
- Minimum interactions with the game.
- Multiple langage support : EN, FR, DE, ES, NL, CZ.
- InfoBox giving the multiple states of the tool.
- Pause the writing on demand.
- Stops the writing on demand.
- Rewrite the last slice of text on demand.
- Retrieves the last text written into the ChatBox.
- Customizable keys.
- Customizable background.
- Customizable style (knowledge in QSS required).
- [Experimental] Change background each time the windows are switching.

## Installation

To install Gw2 - ChatBoxEx, you just can put it anywhere you want on your computer. You do not need to put it into the Guild Wars 2 folder to make it work.\
Once Gw2 - ChatBoxEx is in place, make sure that you have the recommended files into your folder. Check the file section below to know more.\
To change the language, customize the keys or enabling feature, open chatboxex.properties. You can open it with any text editor.\
To customize the interface, you can modify the style.qss file. The QSS format is close from the CSS with some particularities.

## Usage

### Prerequisites

Before starting Gw2 - ChatBoxEx make sure that Guild Wars 2 is already started in window or in Windowed Fullscreen! The program will not work correctly in Full Screen.\
Wait until the ChatBox appears and go back into the game. The program is ready!

### Writing

- To open the ChatBox if it is already visible, click on it, or alt-tab on it. If the ChatBox is closed, summon it by pressing SHIFT+ENTER (default).
- To write the text in-game, press ENTER (default)
- The ChatBox support copy/paste and paragraph. For a new line, you just must press SHIFT+ENTER. This will not open a new ChatBox.
If you want to make an emote, you just need to write the same way as you would do in-game. The program will automatically handle the emotes for each part of the text. Be aware that, for the time being, you can only put one emote per messages. Please, use “short” emotes instead of their longer counterpart, otherwise, some text may not be written entirely.

Example:

“/e likes pastas.” -> Works, recommended\
“/emote likes pastas” -> Works, not recommended.\
“/p Hey people! [newline] /s I’m fine.” -> Does not work.


 
- To stop the writing, press F10 (default). The program will stop the writing process. When “Stopped” is displayed, the process has been stopped and you can use the ChatBox again.\
- To pause the writing, press F9 (default). To continue, press again on the F9 (default) key. The timer will continue to work until it goes to zero, after that, the writing continues when the pause is removed. This can only be used while the program is writing.\
- To re-write the last slice of text, press F8 (default). The text will be written after the timer ends and continues. It is recommended to use this command with “Pause.”. Be aware that redo multiple times the same text will trigger the anti-spam system\
- To retrieve the last text you wrote, press F7 (default), it will be displayed on the ChatBox once you open it again.\
- To quit the program, press SHIFT+ESC (default).


## Important notes

- You can move around while it is writing but it is not recommended.
- You **MUST** have the ChatBox closed when the writing starts. A future version will correct this with a mumble integration.
- The program heavily uses the clipboard. Because of technical limitation, it was not possible to restore it without risking a file corruption or a pure crash of the application. The clipboard is completely cleaned each time something is written. Do NOT keep important information in the clipboard while using the program. 

## Files

### Light version

The program can work in standalone, but for a more comfortable use, this one requires multiple files and folder. Make sure that everything is present.\
Be aware that this version takes far more time to load than the normal one. On a modern system, it takes about 15 seconds to load.\
Here is a little diagram show the following files and folders required for the best ease of use:

![ChatBoxEx Light](readme_img/gw2_folder_config_light.png "ChatBoxEx Light")

### Normal version

The normal version requires a lot more files to work properly but it has the same architecture as the light one:

![ChatBoxEx](readme_img/gw2_folder_config_normal.png "ChatBoxEx")

### Files & Folders description

#### Files

-	Gw2 - ChatBoxEx executable: This is the executable. It can work by itself without any other file, but the program will not be as pleasant to use.
-	language.xml: Contains the whole translations of the program, without this, the default language is set to English.
-	chatboxex.properties: Contains the selected language for the executable, the customizable keys, timers, and experimental feature, without it, the program works with multiple default values described below.
-	style.qss: Pimp the executable to give him a pleasant look and needs the images in the Images folder to change the background.

#### Folders

-	images: The name of the folder explains itself.
-	chatbox_backgrounds: Keep all the background images of the ChatBox. You can add or remove images that you want /do not want to see.
-	icons: Keep the icon of the game. Only used for error message windows. Do not change the name of the file.
-	infobox_backgrounds: Keep all the background images of the InfoBox. You can add or remove images that you want /do not want to see.
-	data: All parameters and properties are inside this folder. 

The background images are selected randomly in the folder each time the program starts, or, if the feature is enabled, each time the Windows are switching.\
If you want to use a custom background, you must know that the size of the ChatBox is relative to your resolution. If takes 35% of the width of the screen and 15% of its height.\
Example for a 16:9 ratio screen:

| Screen resolution | ChatBox resolution |
| ------ | ------ |
| 7680 x 4320 (UHD 8K) | 2688 x 648 |
| 3840 x 2160 (UHD 4K) | 1344 x 324 |
| 2560 x 1440 (QHD 1440p) | 896 x 216 |
| 1920 x 1080 (FULL HD 1080p) | 672 x 162 |
| 1280 x 720 (HD 720p) | 448 x 108 |


## States

The Chat Box has multiple states indicated in real-time by the Info Box. The states are the followings:

### Ready

The chat box is ready and awaits to be use.

### Writing

The chat box is currently writing. It is impossible to use the chat box.

### Pause

The writing process is paused and wait until the pause is over. The timer keeps going while it is paused until its end.\
Work only when the program is writing.

### Stopping

The writing is process is stopping. During this moment, the program is stopping some parts of his process and finishes a couple of actions. It is impossible to use the chat box during this state.\
Work only when the program is writing.

### Stopped

The writing process has been stopped. The chat box is available again.

### Redo

Rewrites the last part of the text instead of sending the next one. This functionality is useful when the last sentence has been blocked by the in-game anti-spam system. It is recommended to use the redo only once per sentences, maybe twice. If you redo too much the same text, the anti-spam system will completely block it for a while.\
It is recommended to use the Redo state with the Pause, so you can have a better control on the time is takes to be sent.\
If you have too much blocked message by the anti-spam system, it is recommended to higher the values of the waiting time per text sent.
Work only when the program is writing.

### Last text

Set the last text on the chat box when opened instead of showing a blank input. Be aware that you must use this state before opening the chat box.

## Default parameters

### Language

By default, the language is set to English (en). You can configure it for the following languages:\
French (fr), German (de), Spanish (es), Dutch (nl), Czech (cz).\
If you the desired language is not available and you know how to translate those ones, please, contact me so I can add it to the program.

### Keys

By default, the key configuration is the following:
-	Exit: SHIFT+ESC
-	Open the ChatBox: SHIFT+ENTER
-	Confirm and write: ENTER
-	Pause: F9
-	Stop: F10
-	Redo: F8
-	Get last text: F7
-	In-game chat box key (important to write): ENTER

### Timers

By default, the timers have the following configuration:

-	Minimum time to wait: 1.00 sec
-	Waiting time per character writing: 0.05 sec (or 50 ms)

### Experimental features

By default, the experimental feature to change the background randomly at switch is disabled for performance reasons.

## MIT License

Copyright (c) 2021 Ryarralk

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
